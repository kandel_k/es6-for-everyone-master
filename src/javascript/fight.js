export function fight(firstFighter, secondFighter) {
  let firstFighterHealth = firstFighter.health;
  let secondFighterHealth = secondFighter.health;

  for (let i = 0;; i++) {

    if (i % 2 == 0) {
      secondFighterHealth -= getDamage(firstFighter, secondFighter);
    } else {
      firstFighterHealth -= getDamage(secondFighter, firstFighter);
    }

    if (firstFighterHealth <= 0) {

      return secondFighter;
    }
    if (secondFighterHealth <= 0) {

      return firstFighter;
    }
  }
}

export function getDamage(attacker, enemy) {
  const damage = getHitPower(attacker) - getBlockPower(enemy);

  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  const criticalChance = Math.random() + 1;

  return fighter.attack * criticalChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;

  return fighter.defense * dodgeChance;
}
