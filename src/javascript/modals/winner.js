import {createElement} from "../helpers/domHelper";
import { showModal } from './modal';

export  function showWinnerModal(fighter) {
  const { name, source } = fighter;
  const title = 'Winner';
  const attributes = { style: 'flex-direction: column; align-items: center' };

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body', attributes });
  const imageDetails = createElement({ tagName: 'div', className: 'image-body' });

  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: { src: source } });

  imageDetails.append(imageElement);
  nameElement.innerText = name;

  fighterDetails.append(imageDetails);
  fighterDetails.append(nameElement);

  showModal({ title, bodyElement: fighterDetails });
}