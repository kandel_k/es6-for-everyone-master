import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, health, defense, source } = fighter;
  const attributes = { style: 'justify-content: space-around;' };

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body', attributes });
  const fighterInfo = createElement({ tagName: 'div', className: 'fighter-info' });

  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: { src: source } });

  nameElement.innerText = 'Name: ' + name;
  fighterInfo.append(nameElement);

  attackElement.innerText = 'Attack: ' + attack;
  fighterInfo.append(attackElement);

  defenseElement.innerText = 'Defense: ' + defense;
  fighterInfo.append(defenseElement);

  healthElement.innerText = 'Health: ' + health;
  fighterInfo.append(healthElement);

  fighterDetails.append(imageElement);
  fighterDetails.append(fighterInfo);

  return fighterDetails;
}
